import shapes.*;

import java.awt.*;
import java.awt.Shape;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class ArrayOfShapes {

    List<shapes.Shape> shapesArray = new ArrayList<shapes.Shape>();

    public ArrayOfShapes() {
        shapesArray = new ArrayList<shapes.Shape>();
    }

    public void insertShape(shapes.Shape shape){
        shapesArray.add(shape);
    }

    public void draw (Graphics g) {
        for( shapes.Shape shape : shapesArray ){
            shape.draw(g);
        }
    }
}

