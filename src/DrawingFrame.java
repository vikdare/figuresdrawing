import javax.swing.*;
import java.awt.*;


public class DrawingFrame extends JFrame{
    
    private ArrayOfShapes list;
    
    public DrawingFrame(ArrayOfShapes list){
        super("Figures");
        setSize(new Dimension(800, 600));
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());
        this.list = list;
        setVisible(true);
    }
    
    @Override
    public void paint(Graphics g) {
        list.draw(g);
    }
    
}
