import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.*;

import shapes.Shape;

public class Graphic extends JFrame {

    private ArrayOfShapes arrayOfShapes;
    private ArrayList<String> shapesNamesList;
    ButtonGroup buttonGroup;
    TextArea textArea;

    public Graphic() {
        super("Figures");
        arrayOfShapes = new ArrayOfShapes();
        getShapeClassesArray();
        createGUI();

    }

    private void createGUI()
    {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        buttonGroup = new ButtonGroup();
        setBackground(Color.YELLOW);
        for (String shapeName : shapesNamesList){
            JRadioButton but = new JRadioButton(shapeName);
            buttonGroup.add(but);
            add(but);
        }
        textArea = new TextArea();
        textArea.setPreferredSize(new Dimension(200, 25));
        add(textArea);
        Button drawButton = new Button("Draw");

        drawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drawFigures(e);
            }
        });

        add(drawButton);

        setLayout(new GridLayout(8,1));
        pack();
    }

    private void getShapeClassesArray(){
        shapesNamesList = new ArrayList<String>();
        ArrayList<Class<?>> shapeClassesList = ClassFinder.find("shapes");
        for (Class shapeClass :shapeClassesList){
            try {
                String nameShapeClass = shapeClass.getName();
                shapesNamesList.add(nameShapeClass);

            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void drawFigures(ActionEvent e) {
        try
        {
            Class[] cArg = new Class[1];
            cArg[0] = int[].class;
            for (String shapeName : shapesNamesList){
                if (shapeName.equals(getSelectedButtonText(buttonGroup))){
                    arrayOfShapes.insertShape((Shape) Class.forName(shapeName).getDeclaredConstructor(cArg).newInstance
                            (getOptions()));
                }
            }


        }catch (Exception exc){
            exc.printStackTrace();
        }
        finally {
            DrawingFrame drawingFrame = new DrawingFrame(arrayOfShapes);
            buttonGroup.clearSelection();
        }
    }

    public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }

    public int[] getOptions(){
        String[] optionsString = textArea.getText().split(" ");
        int[] options = new int[optionsString.length];
        for (int i = 0; i < optionsString.length; i++){
            try {
                options[i] = Integer.parseInt(optionsString[i]);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        return options;
    }

    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                new Graphic().setVisible(true);
            }
        });
    }
}
