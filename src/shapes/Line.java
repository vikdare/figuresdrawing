package shapes;

import java.awt.Color;
import java.awt.Graphics;

public class Line extends Shape {
    private int endX;
    private int endY;
    public Line(int[] coordinatesArray) {
        super(coordinatesArray[0], coordinatesArray[1], new Color(coordinatesArray[4]));
        this.endX = coordinatesArray[2];
        this.endY = coordinatesArray[3];
    }


    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.drawLine(startX, startY, endX, endY);
    }
}

