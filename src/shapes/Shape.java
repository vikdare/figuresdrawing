package shapes;

import java.awt.*;

public abstract class Shape {
    public Shape(int startX, int startY, Color shapeColor) {
        this.startX = startX;
        this.startY = startY;
        this.shapeColor = shapeColor;
    }

    public void draw(Graphics g) {
        g.setColor(this.shapeColor);
    }

    int startX, startY;
    Color shapeColor;

}