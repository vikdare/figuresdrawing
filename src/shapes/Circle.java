package shapes;

import java.awt.Color;
import java.awt.Graphics;

public class Circle extends Shape {

    private int width;
    private int height;

    public Circle(int[] coordinatesArray) {
        super(coordinatesArray[0], coordinatesArray[1], new Color(coordinatesArray[4]));
        this.width = coordinatesArray[2];
        this.height = coordinatesArray[3];
    }


    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.drawOval(startX, startY, width, height);
    }

}
