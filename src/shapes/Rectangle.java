package shapes;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle extends Shape {
    public Rectangle(int startX, int startY, Color shapeColor) {
        super(startX, startY, shapeColor);
    }

    int width = 200, height = 300;

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.drawRect(startX, startY, width, height);
    }
}

