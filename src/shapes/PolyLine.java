package shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class PolyLine extends Shape {

    private int[] xPoints;
    private int[] yPoints;

    public PolyLine(int[] coordinatesArray) {
        super(coordinatesArray[1], coordinatesArray[2], new Color(coordinatesArray[0]) );

        if (coordinatesArray.length % 2 == 1){
            xPoints = new int[(coordinatesArray.length-1)/2];
            yPoints = new int[(coordinatesArray.length-1)/2];
            xPoints[0] = startX;
            yPoints[0] = startY;
            int k = 1, j = 1;
            for (int i = 3; i < coordinatesArray.length; i++){
                if (i % 2 == 1) {
                    xPoints[k] = coordinatesArray[i];
                    k++;
                }
                else {
                    yPoints[j] = coordinatesArray[i];
                    j++;
                }
            }
        }
    }


    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.drawPolyline(xPoints, yPoints, xPoints.length);
    }
}

