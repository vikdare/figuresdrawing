package shapes;

import java.awt.Color;
import java.awt.Graphics;

public class RoundRectangle extends Shape {

    private int width, height, arcWidth, arcHeight;

    public RoundRectangle(int[] coordinatesArray) {
        super(coordinatesArray[0], coordinatesArray[1], new Color(coordinatesArray[6]));
        this.width = coordinatesArray[2];
        this.height = coordinatesArray[3];
        this.arcWidth = coordinatesArray[4];
        this.arcHeight = coordinatesArray[5];
    }



    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.drawRoundRect(startX, startY, width, height, arcWidth, arcHeight);
    }
}

